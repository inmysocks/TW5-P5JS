/*\
title: $:/plugins/OokTech/P5JS/P5Widget.js
type: application/javascript
module-type: widget

A widget for using snap.svg in tiddlywiki

NOTE: in the p5.dom.min.js thing you need to replace the '../p5.js' string with the name of the tiddler that has the code.

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var P5, PSDom, myCanvas, drawnObjects, myP5;
var self = this;

var P5Widget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
P5Widget.prototype = new Widget();

/*
Render this widget into the DOM
*/
P5Widget.prototype.render = function(parent,nextSibling) {
	this.parentDomNode = parent;
	this.computeAttributes();

  var domNode = this.document.createElement('div');

  this.width = this.getAttribute("width", 300);
  this.height = this.getAttribute("height", 300);
  this.id = this.getAttribute("id", "p5-div")

  domNode.setAttribute('id',this.id)
  domNode.setAttribute('style',`width:${this.width}px;height:${this.height}px;`)
  this.execute();
  parent.insertBefore(domNode,nextSibling);
	this.renderChildren(domNode,null);
	this.domNodes.push(domNode);
};

/*
Compute the internal state of the widget
*/
P5Widget.prototype.execute = function() {
  drawnObjects = null
  myCanvas = null

  this.type = this.getAttribute("type", "barChart");
  this.data = this.getAttribute("data", "0 1 2 3")

  console.log('1')

  P5 = require("$:/plugins/OokTech/P5JS/P5.js");
  PSDom = require("$:/plugins/OokTech/P5JS/P5DOM.js");
  myP5 = new P5(this.s);
  myP5.type = this.type;
  myP5.id = this.id;
  myP5.width = this.width;
  myP5.height = this.height;
  myP5.data = this.data.split(' ');
};

P5Widget.prototype.s = function (p) {
  p.setup = function () {
    p.noLoop()
  }

  p.draw = function () {
    var thisDiv = document.getElementById(p.id);

    if (thisDiv) {
      if (!myCanvas) {
        myCanvas = p.createCanvas(p.width, p.height);
        myCanvas.parent(p.id);
      }
      if (!drawnObjects) {
        if (p.type === "barChart") {
          drawnObjects = barChart(p.width, p.height, p.data, p)
        } else if (p.type === "pieChart") {
          drawnObjects = pieChart(p.width, p.height, p.data, p)
        }
      }
    } else {
      if (drawnObjects) {
        if (drawnObjects.length) {
          for (var i = 0; i < drawnObjects.length; i++) {
            drawnObjects[i].remove();
          }
        } else {
          drawnObjects.remove();
        }
      }
      drawnObjects = null
      myCanvas = null
      p.removeElements();
    }
  }
}

/*
  Bar Chart
*/
function barChart(width, height, data, p) {
  console.log('bar')
  var bars = [];
  var barWidth = width/data.length;
  for (var i = 0; i < data.length; i++) {
    //Inputs are x, y, width, height
    bars.push(p.rect(i*barWidth, height, barWidth, -1*data[i]));
  }
}

/*
  Pie Chart
*/
function pieChart(diameter, height, data, p) {
  console.log('pie')
  var lastAngle = 0;
  for (var i = 0; i < data.length; i++) {
    var gray = p.map(i, 0, data.length, 0, 255);
    p.fill(gray);
    var width = 400;
    var height = 400;
    p.arc(width/2, height/2, diameter, diameter, lastAngle, lastAngle+p.radians(data[i]));
    lastAngle += p.radians(data[i]);
  }
}

/*
Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
*/
P5Widget.prototype.refresh = function(changedTiddlers) {
	//draw()
};

exports["p5-widget"] = P5Widget;

})();
